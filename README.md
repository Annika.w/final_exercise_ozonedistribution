# ozonedistribution

## Description
A project to investigate the global distribution of ozone in the atmosphere, the vertical profile and the ozone hole.

## Installation
Use the following command in the base directory to install:  
pip install .

If install requiremets cannot load, please try loading them manually.  
For example: 

mamba install cartopy

## Prerequisites:
mamba create -n „name“ python  
mamba activate „name“

## Usage
plot_ozone [-h] [--ozone_distribution] [--vertical_profile] [--ozone_hole]
                  [--timeperiod_distribution {seasonal,all,slice}]
                  [--start_date START_DATE] [--end_date END_DATE]
                  [--timeperiod_hole {seasonal,antarctic_spring}]
                  --filename FILENAME
                  --save_as SAVE_AS

## Additional Notes:
Input ozone data should be in kg kg-1.
