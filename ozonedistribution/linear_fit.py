import numpy as np


def linear_fit(da_to_fit):
    """
    Creates a linear fit.

    Parameters
    ----------
    da_to_fit : xarray DataArray
        Data to be fitted.

    Returns
    -------
    trend : xarray DataArray
        Contains values for the slope and intercept of the linear fit.

    """
    trend = np.polyfit(da_to_fit.time.dt.year, da_to_fit, 1)
    return trend
