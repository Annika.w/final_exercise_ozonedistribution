import numpy as np
import xarray as xr

xr.set_options(keep_attrs=True)


def _altitude_max_ozone(da_ozone_timean):
    """
    Computes height of the maximum ozone.

    Parameters
    ----------
    da_ozone_timean : xarray DataArray
        DataArray with the ozone data.

    Returns
    -------
    height_max_ozone : xarray DataArray
        Processed DataArray.

    """
    condition_max_ozone = da_ozone_timean == da_ozone_timean.max(dim="altitude")
    height_max_ozone_mask = xr.where(
        condition_max_ozone, da_ozone_timean["altitude"], np.nan
    )
    height_max_ozone = height_max_ozone_mask.sum("altitude")
    return height_max_ozone


def altitude_max_ozone_seasonal(ds_input):
    """
    Computes specific height of the maximum ozone over all seasons.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.

    Returns
    -------
    height_max_ozone_transposed : xarray DataArray
        Final DataArray, ready to be plotted.

    """
    da_ozone_timean_seasons = ds_input.ozone.groupby("time.season").mean()

    for season in da_ozone_timean_seasons.season:
        da_ozone_timean_seasons.sel(season=season)
        height_max_ozone = _altitude_max_ozone(da_ozone_timean_seasons)
        height_max_ozone_transposed = height_max_ozone.transpose(
            "season", "latitude_bins", "longitude_bins", transpose_coords=True
        )
    return height_max_ozone_transposed


def altitude_max_ozone_overall(ds_input):
    """
    Computes specific height of the maximum ozone over all time.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.

    Returns
    -------
    height_max_ozone_transposed : xarray DataArray
        Final DataArray, ready to be plotted.

    """
    da_ozone_timean = ds_input.ozone.mean("time")
    height_max_ozone = _altitude_max_ozone(da_ozone_timean)
    height_max_ozone_transposed = height_max_ozone.transpose(
        "latitude_bins", "longitude_bins", transpose_coords=True
    )
    return height_max_ozone_transposed


def altitude_max_ozone_time_slice(ds_input, start_date, end_date):
    """
    Computes specific height of the maximum ozone over selected timeperiod.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.
    start_date : String
        Start date of the timeperiod.
    end_date : String
        End date of the timeperiod.

    Returns
    -------
    height_max_ozone_transposed : xarray DataArray
        Final DataArray, ready to be plotted.

    """
    ds_input_slice = ds_input.sel(time=(slice(start_date, end_date)))
    height_max_ozone_transposed = altitude_max_ozone_overall(ds_input_slice)
    return height_max_ozone_transposed
