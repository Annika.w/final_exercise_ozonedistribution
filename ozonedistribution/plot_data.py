import matplotlib.pyplot as plt
import cartopy.crs as ccrs


def _save(save_as):
    """
    Saves the figure.

    Parameters
    ----------
    save_as : String.
        Where it is saved.

    Returns
    -------
    None.

    """
    plt.savefig(save_as + "/Ozone_plot")


def plot_altitude_max_ozone_seasonal(da_processed, save_as):
    """
    Plot data of altitde where ozone is maximum over the seasons.

    Parameters
    ----------
    da_processed : xarray DataArray
        Processed Data for plotting.
    save_as : String
        Where it is saved.

    Returns
    -------
    None.

    """
    fig, axes = plt.subplots(
        nrows=4, subplot_kw={"projection": ccrs.EqualEarth()}, figsize=(6, 8)
    )

    for i, season in enumerate(("DJF", "MAM", "JJA", "SON")):
        ozone_plot = da_processed.sel(season=season).plot.pcolormesh(
            ax=axes[i],
            transform=ccrs.PlateCarree(),
            vmin=30000,
            vmax=38000,
            add_colorbar=False,
            extend="both",
        )

    for n, ax in enumerate(axes):
        ax.coastlines()
        grid_lines = ax.gridlines(draw_labels=True, linestyle="dashed")
        grid_lines.top_labels = False
        grid_lines.right_labels = False

    cax = plt.axes([0.85, 0.05, 0.04, 0.85])
    fig.colorbar(ozone_plot, cax=cax, ax=axes[:], shrink=0.8)
    plt.suptitle("Height of maximum Ozone Level", x=0.55, y=0.97, fontsize=16)
    plt.subplots_adjust(
        left=0.1, bottom=0.05, right=0.8, top=0.9, wspace=0.4, hspace=0.4
    )
    _save(save_as)
    plt.show()


def plot_altitude_max_ozone(da_processed, save_as):
    """
    Plot data of altitde where ozone is maximum over all time.

    Parameters
    ----------
    da_processed : xarray DataArray
        Processed Data for plotting.
    save_as : String
        Where it is saved.

    Returns
    -------
    None.

    """
    plt.figure(figsize=(7, 5))
    ax = plt.axes(projection=ccrs.EqualEarth())
    da_processed.plot(ax=ax, transform=ccrs.PlateCarree())
    ax.coastlines()
    grid_lines = ax.gridlines(draw_labels=True, linestyle="dashed")
    grid_lines.top_labels = False
    grid_lines.right_labels = False

    plt.title("Height of maximum Ozone Level", fontsize=18)
    _save(save_as)
    plt.show()


def plot_vertical_profile(ds_processed, save_as):
    """
    Plot vertical profile of ozone at different latitude zones.

    Parameters
    ----------
    ds_processed : xarray DataSet
        Processed Data for plotting.
    save_as : String
        Where it is saved.

    Returns
    -------
    None.

    """
    plt.figure(figsize=(6, 7))
    y_axis = ds_processed["altitude"]

    for zone in (
        "ozone_worldwide",
        "ozone_tropics",
        "ozone_midlat_south",
        "ozone_midlat_north",
        "ozone_highlat_south",
        "ozone_highlat_north",
    ):
        plt.plot(ds_processed[zone], y_axis, label=(f"{zone}"), linewidth=1.5)

    plt.xlabel("Ozone Concentration (ppm)")
    plt.ylabel("Altitude (m)")
    plt.title("Mean Ozone Concentrations for different Latitude Zones")
    plt.legend()
    _save(save_as)
    plt.show()


def plot_ozone_hole_seasonal(da_processed, save_as):
    """
    Plot seasonal ozone hole pattern over Antactica.

    Parameters
    ----------
    da_processed : xarray DataArray
        Processed Data for plotting.
    save_as : String
        Where it is saved.

    Returns
    -------
    None.

    """
    colors = {3: "lightgreen", 6: "orange", 9: "brown", 12: "lightblue"}
    seasons = {3: "spring", 6: "summer", 9: "fall", 12: "winter"}

    fig, ax = plt.subplots(figsize=(12, 6))
    for month, fig in da_processed.groupby("time.month"):
        fig.plot(
            ax=ax,
            color="grey",
            marker="o",
            markerfacecolor=colors[month],
            markeredgecolor=colors[month],
            label=seasons[month],
        )

    plt.title(
        "Seasonal Change of the Ozone Concentration over Antarctica"
        " at 14 to 21 km Altitude"
    )
    plt.legend(bbox_to_anchor=(1, 1))
    _save(save_as)
    plt.show()


def plot_ozone_hole_max_season(da_processed, da_trend, save_as):
    """
    Plot SON ozone hole pattern over Antactica with trendline.

    Parameters
    ----------
    da_processed : xarray DataArray
        Processed Data for plotting.
    da_trend : xarray DataArray
        Values for trend line
    save_as : String
        Where it is saved.

    Returns
    -------
    None.

    """
    y_axis = da_processed.time.dt.year

    plt.subplots(figsize=(10, 4))
    plt.plot(
        y_axis,
        da_processed,
        color="grey",
        marker="o",
        markerfacecolor="brown",
        markeredgecolor="brown",
        label="Ozone values",
    )

    plt.plot(
        y_axis,
        da_trend[1] + da_trend[0] * y_axis,
        "r",
        label=f"Slope trendline: {da_trend[0]:.6f}",
    )
    plt.title = "Mean Ozone Concentration over Antarctica (SON)"
    plt.legend(loc="upper left")
    _save(save_as)
    plt.show()
