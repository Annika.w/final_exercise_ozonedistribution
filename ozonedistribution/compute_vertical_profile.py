import xarray as xr


def vertical_profiles_lat_zones(ds_input):
    """
    Computes the vertical profile of ozone in ppm of different latitude zones.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.

    Returns
    -------
    ds_all_zones : xarray Dataset
        Ozone values for different latitude zones.

    """

    worldwide = slice(-85, 85)
    tropics = slice(-30, 30)
    midlat_south = slice(-60, -30)
    midlat_north = slice(30, 60)
    highlat_south = slice(-85, -60)
    highlat_north = slice(60, 85)
    zones = [
        "worldwide",
        "tropics",
        "midlat_south",
        "midlat_north",
        "highlat_south",
        "highlat_north",
    ]
    ds_lat_zones = xr.Dataset()

    for i, zone in enumerate(
        (worldwide, tropics, midlat_south, midlat_north, highlat_south, highlat_north)
    ):
        ds_zones = ds_input.ozone.sel(latitude_bins=zone)
        da_zones = ds_zones.mean("latitude_bins").mean("time").mean("longitude_bins")
        ds_lat_zones = ds_lat_zones.assign({f"ozone_{zones[i]}": da_zones})

    return ds_lat_zones
