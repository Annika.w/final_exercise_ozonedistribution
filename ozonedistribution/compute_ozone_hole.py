def ozone_hole_seasonal_pattern(ds_input):
    """
    Computes the seasonal pattern of the Antarctic ozone hole between 14-21km.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.

    Returns
    -------
    da_ozone_hole_seasonal : xarray DataArray
        Seasonal ozone hole pattern in ppm over Antarctica.

    """
    da_ozone_hole = ds_input.ozone.sel(latitude_bins=(slice(-85, -66))).sel(
        altitude=(slice(14000, 21000))
    )
    da_ozone_hole_mean = (
        da_ozone_hole.mean("latitude_bins").mean("altitude").mean("longitude_bins")
    )

    da_ozone_hole_seasonal = da_ozone_hole_mean.resample(time="QS-DEC").mean()
    return da_ozone_hole_seasonal


def ozone_hole_max_season(ds_input):
    """
    Computes the timeseries of the Antarctic spring ozone hole between 14-21km.

    Parameters
    ----------
    ds_input : xarray Dataset
        Initial unaltered Dataset.

    Returns
    -------
    da_ozone_son : xarray DataArray
        Spring ozone hole time series in ppm over Antarctica.

    """
    da_ozone_hole_seasonal = ozone_hole_seasonal_pattern(ds_input)
    da_ozone_hole_seasonal.groupby("time.month")
    da_ozone_son = da_ozone_hole_seasonal[da_ozone_hole_seasonal.time.dt.month == 9]
    return da_ozone_son
