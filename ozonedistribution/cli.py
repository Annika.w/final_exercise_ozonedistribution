import argparse
from pathlib import Path

from . import (
    select_data,
    linear_fit,
    compute_height_max_ozone,
    compute_vertical_profile,
    compute_ozone_hole,
    plot_data,
)


def ozonedistribution():
    """Entry point for the ozonedistribution module"""
    parser = argparse.ArgumentParser(
        description="Program which visualizes global ozone distribution."
    )
    parser.add_argument(
        "--ozone_distribution",
        "-o_d",
        action="store_true",
        help="Calculate ozone distribution.\n Attention: "
        "In addition, timeperiod_distribution has to be selected.",
    )
    parser.add_argument(
        "--vertical_profile",
        "-v_p",
        action="store_true",
        help="Calculate vertical profile of ozone.",
    )
    parser.add_argument(
        "--ozone_hole",
        "-o_h",
        action="store_true",
        help="Calculate ozone hole over Antarctica."
        "\n Attention: In addition, timeperiod_hole has to be selected.",
    )
    parser.add_argument(
        "--timeperiod_distribution",
        "-t_d",
        choices=["seasonal", "all", "slice"],
        action="store",
        help="Choose to calculate over all time (=all), "
        "seasonal (=seasonal) or individuel time slice (=slice).\n Attention: "
        "When choice is slice, start_date and end_date need to be selected.",
    )
    parser.add_argument(
        "--start_date",
        "-s_d",
        action="store",
        type=str,
        help="Start date (format: '2010-01-01')",
    )
    parser.add_argument(
        "--end_date",
        "-e_d",
        action="store",
        type=str,
        help="End date (format: '2011-01-01')",
    )
    parser.add_argument(
        "--timeperiod_hole",
        "-t_h",
        choices=["seasonal", "antarctic_spring"],
        action="store",
        help="Choose to calculate seasonal (=seasonal) "
        "or Antarctic spring (=antarctic_spring).",
    )
    parser.add_argument(
        "--filename",
        "-fn",
        required=True,
        help="Select a file eg. 'path/filename'.",
    )
    parser.add_argument(
        "--save_as",
        "-s",
        required=True,
        help="Choose where it is saved.",
    )
    args = parser.parse_args()

    if not Path(args.filename).exists():
        parser.error(f"File {args.filename} does not exist.")

    if not Path(args.save_as).exists():
        parser.error(f"Output directory {Path(args.save_as)} does not exist.")

    if args.ozone_distribution and not args.timeperiod_distribution:
        parser.error("-t_d, --timeperiod_distribution needs to be selected as well.")

    if (
        args.ozone_distribution
        and args.timeperiod_distribution == "slice"
        and (args.start_date is None or args.end_date is None)
    ):
        parser.error("-s_d and -e_d, need to be selected for choice 'slice'.")

    if args.ozone_hole and not args.timeperiod_hole:
        parser.error("-t_h, --timeperiod_hole needs to be selected as well.")

    if args.ozone_distribution and args.timeperiod_distribution == "seasonal":
        ds_input = select_data.select_data(args.filename)
        height_max_ozone_seasonal = (
            compute_height_max_ozone.altitude_max_ozone_seasonal(ds_input)
        )
        plot_data.plot_altitude_max_ozone_seasonal(
            height_max_ozone_seasonal, args.save_as
        )
    elif args.ozone_distribution and args.timeperiod_distribution == "all":
        ds_input = select_data.select_data(args.filename)
        height_max_ozone_overall = compute_height_max_ozone.altitude_max_ozone_overall(
            ds_input
        )
        plot_data.plot_altitude_max_ozone(height_max_ozone_overall, args.save_as)
    elif args.ozone_distribution and args.timeperiod_distribution == "slice":
        ds_input = select_data.select_data(args.filename)
        height_max_ozone_slice = compute_height_max_ozone.altitude_max_ozone_time_slice(
            ds_input, args.start_date, args.end_date
        )
        plot_data.plot_altitude_max_ozone(height_max_ozone_slice, args.save_as)

    if args.vertical_profile:
        ds_input = select_data.select_data(args.filename)
        ds_input_ppm = select_data.to_ppm(ds_input)
        ds_lat_zones = compute_vertical_profile.vertical_profiles_lat_zones(
            ds_input_ppm
        )
        plot_data.plot_vertical_profile(ds_lat_zones, args.save_as)

    if args.ozone_hole and args.timeperiod_hole == "seasonal":
        ds_input = select_data.select_data(args.filename)
        ds_input_ppm = select_data.to_ppm(ds_input)
        da_ozone_hole_seasonal = compute_ozone_hole.ozone_hole_seasonal_pattern(
            ds_input_ppm
        )
        plot_data.plot_ozone_hole_seasonal(da_ozone_hole_seasonal, args.save_as)
    elif args.ozone_hole and args.timeperiod_hole == "antarctic_spring":
        ds_input = select_data.select_data(args.filename)
        ds_input_ppm = select_data.to_ppm(ds_input)
        da_ozone_son = compute_ozone_hole.ozone_hole_max_season(ds_input_ppm)
        da_trend = linear_fit.linear_fit(da_ozone_son)
        plot_data.plot_ozone_hole_max_season(da_ozone_son, da_trend, args.save_as)
