import xarray as xr


def select_data(filename):
    """
    Select data and specific variabel.

    Parameters
    ----------
    filename : String
        Select what file should be read.

    Returns
    -------
    ds : xarray.Dataset
        Dataset including data.

    """
    with xr.open_dataset(filename) as ds_input:
        ds_input.load()
    return ds_input


def to_ppm(ds_input):
    """
    Transforms unit of ozone to ppm.

    Parameters
    ----------
    ds_input : xarray.Dataset
        Dataset including data.

    Returns
    -------
    ds_input_ppm : xarray.Dataset
        Ozone data now in ppm.

    """
    ds_input_ppm = ds_input * (10**6)
    ds_input_ppm["ozone"].attrs["units"] = "ppm"
    return ds_input_ppm
